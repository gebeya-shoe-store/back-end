from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from . import models
from . import serializers

# Create your views here.

class TransactionView(viewsets.ModelViewSet):
    """ Handles listing, creating and updating Part Location """

    queryset = models.Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer


@api_view(['POST'])
def create_transaction(request):

    serializer = serializers.TransactionSerializer(data=request.data)

    if serializer.is_valid():
        return Response("ok")
    else:
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)