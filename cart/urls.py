from django.urls import path, include

from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()

router.register('transactions', views.TransactionView)

urlpatterns = [
    path('', include(router.urls)),
    # path('transactions', views.create_transaction, name = "create_transaction"),
]