from django.db import models
from django.core.validators import MinValueValidator
from django.core.validators import RegexValidator

# Create your models here.
from shoe import models as shoe_models


class ItemCollections(models.Model):
    """ A item and amount collection model """
    item                        = models.ForeignKey(shoe_models.Products, on_delete = models.PROTECT)
    amount                      = models.IntegerField(validators=[MinValueValidator(0)], default = 1)




class Transaction(models.Model):
    """ A transaction model to record user purchases """

    first_name                  = models.CharField(max_length = 255)
    last_name                   = models.CharField(max_length = 255)
    phone_number                = models.CharField(max_length = 11, validators = [RegexValidator(regex=r'^09[0-9]{8}$')])
    email                       = models.EmailField(max_length = 255, blank = True, null = True)
    location_description        = models.TextField(null = True, blank = True)
    item_collections            = models.ManyToManyField(ItemCollections)
    price                       = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0.01)])
    currency                    = models.ForeignKey(shoe_models.Currency, on_delete = models.PROTECT)

    created                     = models.DateTimeField(auto_now_add=True)
    updated                     = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.first_name + " " + self.last_name 