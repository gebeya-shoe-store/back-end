from rest_framework import serializers
from django.db import transaction

from . import models


class ItemCollectionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ItemCollections
        fields = ('__all__')


class TransactionSerializer(serializers.ModelSerializer):

    item_collections = ItemCollectionsSerializer(many = True)

    class Meta:
        model = models.Transaction
        fields = ('__all__')

    @transaction.atomic
    def create(self, validated_data):
        transaction = models.Transaction.objects.create(first_name = validated_data['first_name'],
                                                        last_name = validated_data['last_name'],
                                                        phone_number = validated_data['phone_number'],
                                                        email = validated_data['email'],
                                                        location_description = validated_data['location_description'],
                                                        price = validated_data['price'],
                                                        currency = validated_data['currency'])
        for collection in validated_data['item_collections']:
            transaction.item_collections.add(models.ItemCollections.objects.create(
                item = collection['item'],
                amount = collection['amount']
            ))


        transaction.save()
        return transaction