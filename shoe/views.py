from django.shortcuts import render

from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from . import models
from . import serializers


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    # page_size_query_param = 'page_size'
    # max_page_size = 1000


class ProductsView(viewsets.ModelViewSet):
    """ a view set to Handles Listing Products """
    
    queryset = models.Products.objects.all()
    serializer_class = serializers.ProductsSerializer
    filter_backends = (SearchFilter, DjangoFilterBackend)
    search_fields = ('name', 'brand__name', 'shoe_type__name')
    filterset_fields = ('brand', 'shoe_type', 'sex')
    pagination_class = StandardResultsSetPagination


class CurrencyView(viewsets.ModelViewSet):
    """ Handles listing, creating and updating Part Location """

    queryset = models.Currency.objects.all()
    serializer_class = serializers.CurrencySerializer


class BrandView(viewsets.ModelViewSet):
    """ Handles listing, creating and updating Part Location """

    queryset = models.Brand.objects.all()
    serializer_class = serializers.BrandSerializer


class TypeView(viewsets.ModelViewSet):
    """ Handles listing, creating and updating Part Location """

    queryset = models.Type.objects.all()
    serializer_class = serializers.TypeSerializer


@api_view(['GET'])
def shoe_filter_data(request):

    brands = models.Brand.objects.all()
    brands_serializer = serializers.BrandSerializer(brands, many = True)

    types = models.Type.objects.all()
    types_serializer = serializers.TypeSerializer(types, many = True)

    currencies = models.Currency.objects.all()
    currencies_serializer = serializers.CurrencySerializer(currencies, many = True)

    return Response({'brand': brands_serializer.data, 
                     'shoe_type': types_serializer.data,
                     'currencies': currencies_serializer.data})
