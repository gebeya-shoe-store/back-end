from rest_framework import serializers

from . import models

class CurrencySerializer(serializers.ModelSerializer):
    """ A serializer for our Currency Object. """

    class Meta:
        model = models.Currency
        fields = ('__all__')

class BrandSerializer(serializers.ModelSerializer):
    """ A serializer for our Brand Object. """

    class Meta:
        model = models.Brand
        fields = ('__all__')

class TypeSerializer(serializers.ModelSerializer):
    """ A serializer for our Type Object. """

    class Meta:
        model = models.Type
        fields = ('__all__')

class ProductsSerializer(serializers.ModelSerializer):
    """ A serializer for our shoe product object. """

    currency = CurrencySerializer()
    brand = BrandSerializer()
    shoe_type = TypeSerializer()

    class Meta:
        model = models.Products
        fields = ('__all__')
