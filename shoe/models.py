from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

class Currency(models.Model):
    """ A Currency model to hold currency type """
    name                = models.CharField(max_length = 255, unique = True)
    short_name          = models.CharField(max_length = 255, unique = True)
    country             = models.CharField(max_length = 255, unique = True)
    relation_to_dollar  = models.DecimalField(max_digits=10, decimal_places=3, validators=[MinValueValidator(0.01)])

    class Meta:
        verbose_name = 'Currency'
        verbose_name_plural = 'Currency'

    def __str__(self):
        return self.name + ' of ' + self.country


class Brand(models.Model):
    """ A Brand model to hold brand type """
    name                = models.CharField(max_length = 255, unique = True)

    def __str__(self):
        return self.name

class Type(models.Model):
    """ A Brand model to hold brand type """
    name                = models.CharField(max_length = 255, unique = True)

    def __str__(self):
        return self.name


SEX = (
    ('man', 'Man'),
    ('woman', 'Woman'),
)

class Products(models.Model):
    """ A product model to hold our shoe data """
    name            = models.CharField(max_length = 255, unique = True)
    price           = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0.01)])
    currency        = models.ForeignKey(Currency, on_delete = models.PROTECT)
    brand           = models.ForeignKey(Brand, on_delete = models.PROTECT, null = True, blank = True)
    shoe_type       = models.ForeignKey(Type, on_delete = models.PROTECT, null = True, blank = True)
    sex             = models.CharField(max_length = 10, choices = SEX, default = 'man')
    image           = models.ImageField(upload_to='shoe/', blank=True)    

    created         = models.DateTimeField(auto_now_add=True)
    updated         = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return str(self.name) + ": " + str(self.price) + " " + self.currency.short_name