from django.contrib import admin

# Register your models here.
from . import models

admin.site.register(models.Products)
admin.site.register(models.Currency)
admin.site.register(models.Brand)
admin.site.register(models.Type)