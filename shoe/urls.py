from django.urls import path, include

from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()

router.register('shoes', views.ProductsView)
router.register('currency', views.CurrencyView)
router.register('brand', views.BrandView)
router.register('type', views.TypeView)

urlpatterns = [
    path('', include(router.urls)),
    path('shoe-filter-data', views.shoe_filter_data, name = "shoe_filter_data"),
]